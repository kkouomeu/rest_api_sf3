<?php
/**
 * Created by PhpStorm.
 * User: arsenekevin
 * Date: 18/08/2018
 * Time: 20:21
 */

namespace AppBundle\Controller;

use AppBundle\Form\Type\PlaceType;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; //Alias for annotations
use AppBundle\Entity\Place;


class PlaceController extends Controller
{

    /**
     * @Rest\View(serializerGroups={"place"})
     * @Rest\Get("/places")
     */
    public function getPlacesAction(){

        //retrieve all place in array value $places
        $places = $this->get('doctrine.orm.entity_manager')
                       ->getRepository('AppBundle:Place')
                       ->findAll();

        return $places;
    }


    /**
     * @Rest\View(serializerGroups={"place"})
     * @Rest\Get("/places/{id}")
     */
    public function getPlaceAction(Request $request)
    {
        //retrieve one place in array value $place
        $place = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Place')
            ->find($request->get('id'));

        if (empty($place)) {
            return $this->placeNotFound();
        }

        return $place;
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED,serializerGroups={"place"})
     * @Rest\Post("/places")
     */
    public function postPlacesAction(Request $request){

        $place = new Place();

        //Ici on verifie les données envoyées par le client avec le form symfony ( we get value foreach key )
        $form = $this->createForm(PlaceType::class,$place);
        $form->submit($request->request->all()); //Validation of datas

        if ($form->isValid()){
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($place);
            $em->flush();

            return $place;
        } else {
            return $form;
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT,serializerGroups={"place"})
     * @Rest\Delete("/places/{id}")
     */
    public function removePlaceAction(Request $request)
    {
        //delete one place
        $em = $this->get('doctrine.orm.entity_manager');
        $place = $em->getRepository('AppBundle:Place')
                    ->find($request->get('id'));

        if(empty($place)){
           return $this->placeNotFound();
        }

        foreach ($place->getPrices() as $price){
            $em->remove($price);
        }

        $em->remove($place);
        $em->flush();

        return $this->placeHasDeleted();

    }

    /**
     * @Rest\View(serializerGroups={"place"})
     * @Rest\Put("/places/{id}")
     */
    public function updatePlaceAction(Request $request)
    {
        return $this->updatePlace($request, true);
    }


    /**
     * @Rest\View(serializerGroups={"place"})
     * @Rest\Patch("/places/{id}")
     */
    public function patchPlaceAction(Request $request)
    {
        return $this->updatePlace($request, false);
    }


    //Update Method with $clearMissing parameter
    private function updatePlace(Request $request, $clearMissing)
    {
        $place = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Place')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $place Place */

        if (empty($place)) {
            return $this->placeNotFound();
        }


        //Ici on verifie les données envoyées par le client avec le form symfony ( we get value foreach key )
        $form = $this->createForm(PlaceType::class, $place);

        // Le paramètre false dit à Symfony de garder les valeurs dans notre
        // entité si l'utilisateur n'en fournit pas une dans sa requête
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($place);
            $em->flush();
            return $place;
        } else {
            return $form;
        }
    }

    //place deleted
    private function placeHasDeleted()
    {
        return \FOS\RestBundle\View\View::create(['message' => 'Place has been deleted'], Response::HTTP_NOT_FOUND);
    }

    //place not found
    private function placeNotFound()
    {
        return View::create(['message' => 'Place not found'], Response::HTTP_NOT_FOUND);
    }
}