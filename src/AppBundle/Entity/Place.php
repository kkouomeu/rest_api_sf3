<?php
/**
 * Created by PhpStorm.
 * User: arsenekevin
 * Date: 18/08/2018
 * Time: 20:05
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

//create Entity as Table > Places

/**
 * @ORM\Entity()
 * @ORM\Table(name="places")
 * uniqueConstraints={@ORM\UniqueConstraint(name="places_name_unique",columns={"name"})}
 */
class Place
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $address;

    /**
     * @ORM\OneToMany(targetEntity="Price", mappedBy="place")
     * @var Price[]
     */
    protected $prices;


    /**
     * @ORM\OneToMany(targetEntity="Theme", mappedBy="place")
     * @var Theme[]
     */
    protected $themes;


    public function __construct()
    {
        $this->prices = new ArrayCollection();
        $this->themes = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
    }


    public function getAddress()
    {
        return $this->address;
    }


    public function setAddress($address)
    {
        $this->address = $address;
    }


    public function getPrices()
    {
        return $this->prices;
    }


    public function setPrices($prices)
    {
        $this->prices = $prices;
    }


    public function getThemes()
    {
        return $this->themes;
    }


    public function setThemes($themes)
    {
        $this->themes = $themes;
    }

}